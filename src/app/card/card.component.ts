import { Component, OnInit, Input } from '@angular/core';
import {Card} from '../app.component';
//import { Interpolation } from '@angular/compiler';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],     
    interpolation: ['{{','}}'] 
})
export class CardComponent implements OnInit {

    @Input() card: Card
    @Input() index: number
    
    title = 'My Card Title' //create var for current 'class' 
    text = 'My sample text' // also we cad set a type => text: string = 'My sample text'
   
    cardDate: Date = new Date()
   
    number = 42
    array = [1, 1, 2, 3, 5, 8, 13]
    object = {name: 'Roma', info: {age: 30, job: 'Frontend'}}

    textColor: string

    changeTitle() {
        this.card.title = 'Title has been changed'
    }

    //TWO WAY BUNDING 1:
    // inputHandler(event: any) {
    //     //console.log(event)
    //     const value = event.target.value
    //     this.title = value
    // }

    //TWO WAY BUNDING 2:
    // inputHandler(value) {
    //     this.title = value
    // }

    //TWO WAY BUNDING 3: 
    // work but without using changeHandler method. changeHandler чомусь не спрацював
    // changeHandler() {
    //     console.log(this.title)
    // }



    ngOnInit() {
      
    }
}