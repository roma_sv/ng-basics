import { Component } from '@angular/core';
export interface Card {
  title: string
  text: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-basics';

  toggle = true
  cards: Card[] = [
    {title: 'Card1', text:'This is card number 1'},
    {title: 'Card2', text:'This is card number 2'},
    {title: 'Card3', text:'This is card number 3'},
    {title: 'Card4', text:'This is card number 4'},
    {title: 'Card5', text:'This is card number 5'},
    {title: 'Card6', text:'This is card number 6'},
    {title: 'Card7', text:'This is card number 7'},
    {title: 'Card8', text:'This is card number 8'},
    {title: 'Card9', text:'This is card number 9'}
  ]

  toggleCards() {
    this.toggle = !this.toggle
  }
}